# Часто заддаваемые вопросы по **помощнику заполнения матриц** (FAQ)

<!--toc begin-->
## Содержание документа
<!--toc end-->

**Q: Двойной щелчок на всю папку Дряпки товар не добавился, пришлось раскрыть до корня и отдельно добавлять все бренды**

A: Дерево категорий имеет глубину 3 уровня: мета категория, *первый* уровень(например, **Аквариумистика**); категория, *второй* уровень (например, **Грунты для аквариума**) и **Бренд**, *третий* уровень. Двойной щелчок добавляет все товары из выбранного пункта в таблицу. Т.к. в метакатегории обычно сотни, тысячи товаров, чтобы избежать путаницы, было заблокировано добавление по мета категории.

**Q: Как добавить колонку принадлежности категории**

A: Решение описано в настройке форм в секции [настройка форм: добавление реквизитов](../../../общие_вопросы_по_работе_с_программой/настройка_форм.md#добавление-реквизитов)

**Q: Заметил, что много товара название начинается не от отступа, а с одним или более пробелом, зачем?**

A: НСИ (номенклатура, контрагенты и т.п.) розницы загружается из главной базы организации "как есть" без изменений. Если есть необходимость, ошибки в наименованиях можно исправить в рознице, но (!) если эту позицию перезапишут в главной базе, она автоматически затрет изменения в рознице. В таких ситуациях будет корректно обратиться к ответственному за состояние справочника в главной базе.

**Q: Теоретически десяток человек может создать матрицу одной и той же категории по-разному, не понятно где смотреть что готово, а что нет.**

A: Для анализа и контроля состояния матриц предусмотрен отчет

![ ](./media/1cv8c_GLtSJYy8b0.png)

**Q: Если я случайно закрыл документ без сохранения, то вся работа над матрицей исчезает, в истории изменения матриц документ нашел, но он почему-то проведен и пуст, или не понятно, как открыть его для продолжения работы.**

A: Сохранение "на лету" не предусмотрено, используется стандартная парадигма 1С "сохраняем явно по требованию пользователя". Это продиктовано большим объемом данных в такого рода функционале: при сохранении большого набора информации растет нагрузка на систему. Проблема усугубляется с увеличением количества одновременно работающих пользователей. Касаемо найденного документа: помощник создает/проводит документ *только* по нажатию кнопок "Провести", "Записать". Найденный документ не является ни багом, ни результатом работы. Скорее всего его вводили ранее (например, нажали **Провести** сразу после открытия помощника).

**Q: В режиме выделения выделяется все, зачем?**

A: Использование режима выделения описано в [использование режима выделения таблицы](./readme.md#использование-режима-выделения-таблицы)


